const moment = require('moment')
const remark = require('remark')
const reStripMarkdown = require('strip-markdown')

/**
 * 
 * @param {Object} article Is an object that implements
 * @param {string} article.text Formatted as markdown
 * @param {string} article.title
 * @param {string} article.date Timestamp for last edit
 * 
 * @returns {Object} The synthesized article
 */
module.exports = function (article) {
    const {categories, links} = getInternalLinks(article)

    return {
        title: article.title,
        text: stripMarkdown(article.text),
        date: moment(article.date, 'MMMM DD, YYYY')
                    .format('YYYY-MM-DD'),
        links: links,
        categories: categories,
    }
}

/**
 * Collect links and categories 
 * @param {string} text Some transformed markdown
 * @returns {Object} Of 2 Arrays, categories & links
 */
function getInternalLinks(article) {
    // remove potential formatting within links
    const text = article.text.replace(/\*/, '')

    const markdownLinkRe = /\[(\w+)\]\((\w+)\)/g
    const markdownLinks = [...text.matchAll(markdownLinkRe)]

    const template = {
        'categories': [],
        'links': []
    }    
    
    const internal = markdownLinks.reduce(sortInternal, template)
    
    return {
        categories: [...new Set(internal.categories)],
        links: arrayFrom(countDuplicates(internal.links))
    }

    /**
     * @param {Object} acc
     * @param {Array} match A regex match object
     * @returns Sorted categories and internal links
     */
    function sortInternal(acc, match) {
        const title = match[1]
        if (isInternal(match) && article.title != title) {

            // todo: Strictly, a title isn't enough to determinine if a page.
            //       So, consider the implications of that.
            if (title.startsWith('Category')) {
                acc['categories'].push(`a/${title}`)
            }
            else {
                acc['links'].push(`a/${title}`)
            }
        }

        return acc
    }
}


/**
 * Reduce an array of duplicated items to a map with counts of duplicates
 * @param {Array} arr Array containing duplicates 
 * @returns {Object} {key: title, value: occurences}
 */
function countDuplicates(arr) {
    return arr.reduce((acc, curr) => {
        if (curr in acc) {
          acc[curr]++
        }
        else {
          acc[curr] = 1
        }
        return acc
    }, {})
}


/**
 * 
 * @param {Object} obj {a: 0, b: 1}
 * @returns {Array} [{title: a, count: 0}, {title: b, count: 1}]
 */
function arrayFrom(obj) {
    let entries = Object.entries(obj)
    return entries.map((x) => {
        return {
            title: x[0],
            count: x[1]
        }
    })
}


/*
 * By consequence of markdown link formatting,
 * An internal link looks like `[InternalLink](InternalLink)`
 */
 function isInternal(match) {
    const title = match[1]
    const path = match[2]

    return title == path
}


function stripMarkdown(text) {
    let out = undefined
    remark()
        .use(reStripMarkdown)
        .process(text, function (err, file) {
            if (err) throw err
            out = String(file)
        });

    return out
}
