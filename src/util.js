
const fs = require('fs')
const path = require('path')

/**
 * This is a hack to append objects to a JSON without actually parsing it.
 * @param {Array} x Some unclosed JSON
 * @returns Closed, valid JSON
 */
module.exports.closeJsonArray = function (x) {
    return x.slice(0, -2) + ']'
}

module.exports.multipleOf = function (m, x) {
    return m % x == 0 && m != 0
}

/**
 * Difference of two sets
 * @param {Set} A 
 * @param {Set} B 
 * @returns {Set} A - B
 */
module.exports.difference = function (A, B) {
    return A.filter(x => B.has(x))
}