const functions = require('firebase-functions');
const admin = require('firebase-admin');
const express = require('express');
const exphbs = require('express-handlebars');
const md = require('markdown-it')({ html: true }).use(require('markdown-it-deflist'));

admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    databaseURL: "https://wiiki-0.firebaseio.com"
});

const app = express();
const db = admin.firestore();

app.engine('handlebars', exphbs.engine());
app.set('view engine', 'handlebars');
app.use(express.static('public'));

exports.appv2 = functions.https.onRequest({region: ['us-central1', 'europe-west1']}, app);

// Routes
app.get('/', (req, res) => render('WelcomeVisitors', req, res));
app.get('/:title', (req, res) => res.redirect(`/a/${req.params.title}`));
app.get('/a/:title', (req, res) => render(req.params.title, req, res));

async function render(title, req, res) {
    const article = await renderArticle(title);
    if (!article) {
        return render404(res, title, req.headers['hx-request']);
    }

    renderPage(res, 'article', article, req.headers['hx-request']);
}

function renderPage(res, view, data, isHtmx) {
    res.locals.meta = data.meta;
    res.render(view, { layout: isHtmx ? false : 'main', ...data });
}

function render404(res, title, isHtmx) {
    const data = {
        title: `404: ${title} appears to be missing`,
        meta: isHtmx ? undefined : { title: 'wiiki - 404' }
    };
    res.status(404).render('404', { layout: isHtmx ? false : 'main', ...data });
}

async function renderArticle(title) {
    const doc = await db.collection('articles').doc(title).get();
    const article = doc.exists && doc.data();
    if (!article?.text) return null;

    const content = md.render(article.text);
    return {
        content,
        links: extractWikiLinks(article.text),
        title: title === 'WelcomeVisitors' ? article.title : splitCamelCase(title),
        rawTitle: title,
        date: article.last_modified,
        meta: {
            title: `wiiki - ${title}`,
            description: snippet(content)
        }
    };
}

const splitCamelCase = s => s.replace(/([a-z])([A-Z]+)/g, '$1 $2');

const snippet = text => text
    .substring(0, 250)
    .replace(/(([\/])*\\n([\/])*)+/g, ' ') 
    + ' . . .';

const extractWikiLinks = content => {
    const matches = content.match(/\[\[(.*?)\]\]/g) || [];
    return [...new Set(matches.map(m => m.slice(2, -2)))];
};
