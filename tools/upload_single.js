const fs = require('fs')
const path = require('path')

const admin = require('firebase-admin');

admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    databaseURL: "https://wiiki-0.firebaseio.com"
})

let db = admin.firestore();

let article = {
    "last_modified": 'February 27, 2020',
    "title": 'ChangesInTwentyTwenty',
    'text': "..."
}

const res = db.collection('articles').doc('ChangesInTwentyTwenty').set(article)
res.then(succ => {
    console.log(succ)
})