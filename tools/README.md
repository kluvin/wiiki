This folder contains various tools that are used at various stages.

* *download.sh* contains the script I used to fetch WikiWikiWeb.
* *neo4j.cql* contains the Cypher script used to build the graph database
* *populate_database.js* and *upload_single.js* both upload articles to Firebase. The former uploads the entire set of compiled articles in batches, while the latter uploads a specified article.